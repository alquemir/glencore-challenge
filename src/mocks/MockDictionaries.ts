import { List, Map } from 'immutable';

const mockDictionaries = List([
  {
    id: 'dictionary-1',
    name: 'Colors',
    rows: Map({
      Anthracite: 'Dark Grey',
      'Midnight Black': 'Black',
      'Mystic Silver': 'Silver'
    })
  },
  {
    id: 'dictionary-2',
    name: 'Languages',
    rows: Map({
      Ireland: 'English',
      Chad: 'Arabic',
      Canada: 'French'
    })
  }
]);

export { mockDictionaries };
