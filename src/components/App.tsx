import * as React from 'react';
import { IAppProps } from 'common';
import { getSelectedDictionary } from 'selectors/Dictionaries';
import {
  setSelectedDictionary,
  addDictionary,
  removeDictionary,
  addRowToDictionary,
  deleteRowFromDictionary,
  updateRowInDictionary
} from 'actions/Dictionaries';
import { connect } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { createTheme } from './CreateTheme';
import Grid from '@material-ui/core/Grid';
import Dictionaries from './Dictionaries';
import './App.css';

class App extends React.PureComponent<IAppProps> {
  public render() {
    return (
      <MuiThemeProvider theme={createTheme()}>
        <Grid container className="App">
          <Dictionaries {...this.props} />
        </Grid>
      </MuiThemeProvider>
    );
  }
}

const mapDispatchToProps = {
  setSelectedDictionary,
  addDictionary,
  removeDictionary,
  addRowToDictionary,
  deleteRowFromDictionary,
  updateRowInDictionary
};

const mapStateToProps = ({ dictionaries }: any) => ({
  dictionaries: dictionaries.dictionaries,
  selectedDictionary: getSelectedDictionary(dictionaries)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
