import * as React from 'react';
import { IDictionaryProps, IDictionaryRow } from 'common';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Clear from '@material-ui/icons/Clear';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DictionaryRows from './DictionaryRows';

class Dictionary extends React.PureComponent<IDictionaryProps> {
  public handleExpandClick = () => {
    const { dictionary, setSelectedDictionary } = this.props;
    setSelectedDictionary(dictionary.id);
  };

  public handleClearClick = () => {
    const { dictionary, removeDictionary } = this.props;
    removeDictionary(dictionary.id);
  };

  public handleEditRowClick = (row: IDictionaryRow) => {
    const { dictionary, updateRowInDictionary } = this.props;
    updateRowInDictionary(dictionary.id, row);
  };

  public handleAddRowClick = (row: IDictionaryRow) => {
    const { dictionary, addRowToDictionary } = this.props;
    addRowToDictionary(dictionary.id, row);
  };

  public handleDeleteRowClick = (rowKey: string) => {
    const { dictionary, deleteRowFromDictionary } = this.props;
    deleteRowFromDictionary(dictionary.id, rowKey);
  };

  public render() {
    const { dictionary, isSelected } = this.props;
    return (
      <List disablePadding>
        <ListItem>
          <ListItemText primary={dictionary.name} />
          <ListItemSecondaryAction>
            <IconButton onClick={this.handleClearClick}>
              <Clear />
            </IconButton>
            <IconButton onClick={this.handleExpandClick}>
              {isSelected ? <ExpandLess /> : <ExpandMore />}
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
        <Collapse in={isSelected} timeout="auto" unmountOnExit>
          <DictionaryRows
            dictionary={dictionary}
            onEdit={this.handleEditRowClick}
            onAdd={this.handleAddRowClick}
            onDelete={this.handleDeleteRowClick}
          />
        </Collapse>
      </List>
    );
  }
}

export default Dictionary;
