import * as React from 'react';
import { IDictionariesProps, IDictionariesState, IDictionary } from 'common';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import Dictionary from './Dictionary';
import DictionaryToolbar from './DictionaryToolbar';
import AddDictionary from './AddDictionary';
import EmptyDictionaries from './EmptyDictionaries';
import styles from './DictionariesStyles';

class Dictionaries extends React.PureComponent<
  IDictionariesProps,
  IDictionariesState,
  WithStyles<typeof styles>
> {
  public state = {
    isAddingDictionary: false
  };

  public isSelectedDictionary = (dictionary: IDictionary): boolean => {
    const { selectedDictionary } = this.props;
    return (
      selectedDictionary != null && selectedDictionary.id === dictionary.id
    );
  };

  public handleToolbarAddDictionaryClick = () => {
    this.setState({ isAddingDictionary: true });
  };

  public handleAddDictionary = (name: string) => {
    this.props.addDictionary(name);
    this.setState({ isAddingDictionary: false });
  };

  public render() {
    const { dictionaries, classes } = this.props;
    const { isAddingDictionary } = this.state;
    return (
      <Grid item xs={12}>
        <Grid container justify="center">
          <Grid item xs={7}>
            <DictionaryToolbar onAdd={this.handleToolbarAddDictionaryClick} />
            <List className={classes.list} disablePadding>
              {!dictionaries.isEmpty() ? (
                dictionaries.map((dictionary: IDictionary, index: number) => (
                  <Dictionary
                    key={`dictionary-${index + 1}`}
                    dictionary={dictionary}
                    isSelected={this.isSelectedDictionary(dictionary)}
                    {...this.props}
                  />
                ))
              ) : (
                <EmptyDictionaries />
              )}
              {isAddingDictionary && (
                <AddDictionary onAdd={this.handleAddDictionary} />
              )}
            </List>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(Dictionaries);
