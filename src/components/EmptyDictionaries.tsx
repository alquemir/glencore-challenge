import * as React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const EmptyDictionaries = () => (
  <List disablePadding>
    <ListItem>
      <ListItemText primary="No dictionaries available" />
    </ListItem>
  </List>
);

export default EmptyDictionaries;
