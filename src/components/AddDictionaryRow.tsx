import * as React from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import Done from '@material-ui/icons/Done';
import TextField from '@material-ui/core/TextField';
import { IAddDictionaryRowProps, IAddDictionaryRowState } from 'common';

class AddDictionaryRow extends React.PureComponent<
  IAddDictionaryRowProps,
  IAddDictionaryRowState
> {
  public state = {
    rowFrom: '',
    rowTo: ''
  };

  public handleDoneClick = () => {
    const { rowFrom, rowTo } = this.state;
    this.props.onAdd({ from: rowFrom, to: rowTo });
  };

  public handleRowFromChange = (event: any) => {
    this.setState({ rowFrom: event.target.value });
  };

  public handleRowToChange = (event: any) => {
    this.setState({ rowTo: event.target.value });
  };

  public render() {
    const { validate } = this.props;
    const { rowFrom, rowTo } = this.state;
    return (
      <TableRow>
        <TableCell>
          <TextField
            value={rowFrom}
            onChange={this.handleRowFromChange}
            error={!validate({ from: rowFrom, to: rowTo })}
            label="From"
          />
        </TableCell>
        <TableCell>
          <TextField
            value={rowTo}
            onChange={this.handleRowToChange}
            error={!validate({ from: rowFrom, to: rowTo })}
            label="To"
          />
        </TableCell>
        <TableCell>
          <IconButton onClick={this.handleDoneClick}>
            <Done />
          </IconButton>
        </TableCell>
      </TableRow>
    );
  }
}

export default AddDictionaryRow;
