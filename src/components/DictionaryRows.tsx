import * as React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import { IDictionaryRowsProps, IDictionaryRow } from 'common';
import { isValidDictionaryRow } from 'validation';
import DictionaryRow from './DictionaryRow';
import AddDictionaryRow from './AddDictionaryRow';

class DictionaryRows extends React.PureComponent<IDictionaryRowsProps> {
  public handleEditRowClick = (row: IDictionaryRow) => {
    this.props.onEdit(row);
  };

  public handleAddRowClick = (row: IDictionaryRow) => {
    this.props.onAdd(row);
  };

  public handleDeleteRowClick = (rowKey: string) => {
    this.props.onDelete(rowKey);
  };

  public render() {
    const { dictionary } = this.props;
    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>From</TableCell>
              <TableCell>To</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {dictionary.rows.toArray().map((row: string[], index: number) => (
              <DictionaryRow
                key={`dictionary-row-${index + 1}`}
                row={{ from: row[0], to: row[1] }}
                onEdit={this.handleEditRowClick}
                onDelete={this.handleDeleteRowClick}
              />
            ))}
            <AddDictionaryRow
              onAdd={this.handleAddRowClick}
              validate={isValidDictionaryRow(dictionary)}
            />
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default DictionaryRows;
