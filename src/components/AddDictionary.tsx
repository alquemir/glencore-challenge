import * as React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Done from '@material-ui/icons/Done';
import { IAddDictionaryProps, IAddDictionaryState } from 'common';

class AddDictionary extends React.PureComponent<
  IAddDictionaryProps,
  IAddDictionaryState
> {
  public state = {
    name: ''
  };

  public handleNameChange = (event: any) => {
    this.setState({ name: event.target.value });
  };

  public handleDoneClick = () => {
    const { onAdd } = this.props;
    const { name } = this.state;
    onAdd(name);
  };

  public render() {
    return (
      <List disablePadding>
        <ListItem>
          <TextField onChange={this.handleNameChange} label="Dictionary Name" />
        </ListItem>
        <ListItemSecondaryAction>
          <IconButton onClick={this.handleDoneClick}>
            <Done />
          </IconButton>
        </ListItemSecondaryAction>
      </List>
    );
  }
}

export default AddDictionary;
