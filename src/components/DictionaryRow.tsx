import * as React from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';
import Done from '@material-ui/icons/Done';
import Input from '@material-ui/core/Input';
import Clear from '@material-ui/icons/Clear';
import { IDictionaryRowProps, IDictionaryRowState } from 'common';

class DictionaryRow extends React.PureComponent<
  IDictionaryRowProps,
  IDictionaryRowState
> {
  public state = {
    isEditting: false,
    rowFrom: '',
    rowTo: ''
  };

  public handleEditClick = () => {
    const { row } = this.props;
    this.setState({ isEditting: true, rowFrom: row.from, rowTo: row.to });
  };

  public handleDoneClick = () => {
    const { rowFrom, rowTo } = this.state;
    this.setState({ isEditting: false }, () =>
      this.props.onEdit({ from: rowFrom, to: rowTo })
    );
  };

  public handleDeleteRowClick = () => {
    const { row, onDelete } = this.props;
    onDelete(row.from);
  };

  public handleRowFromChange = (event: any) => {
    this.setState({ rowFrom: event.target.value });
  };

  public handleRowToChange = (event: any) => {
    this.setState({ rowTo: event.target.value });
  };

  public render() {
    const { row } = this.props;
    const { isEditting, rowFrom, rowTo } = this.state;
    return (
      <TableRow>
        <TableCell>
          {isEditting ? (
            <Input value={rowFrom} onChange={this.handleRowFromChange} />
          ) : (
            row.from
          )}
        </TableCell>
        <TableCell>
          {isEditting ? (
            <Input value={rowTo} onChange={this.handleRowToChange} />
          ) : (
            row.to
          )}
        </TableCell>
        <TableCell>
          {isEditting ? (
            <IconButton onClick={this.handleDoneClick}>
              <Done />
            </IconButton>
          ) : (
            <IconButton onClick={this.handleEditClick}>
              <Edit />
            </IconButton>
          )}
          <IconButton onClick={this.handleDeleteRowClick}>
            <Clear />
          </IconButton>
        </TableCell>
      </TableRow>
    );
  }
}

export default DictionaryRow;
