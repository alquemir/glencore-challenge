import * as React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Add from '@material-ui/icons/Add';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import { IDictionaryToolbarProps } from 'common';
import styles from './DictionaryToolbarStyles';

class DictionaryToolbar extends React.PureComponent<
  IDictionaryToolbarProps,
  WithStyles<typeof styles>
> {
  public handleAddClick = () => {
    this.props.onAdd();
  };

  public render() {
    const { classes } = this.props;
    return (
      <AppBar position="static">
        <Toolbar>
          <IconButton
            className={classes.addButton}
            onClick={this.handleAddClick}
            color="inherit"
          >
            <Add />
          </IconButton>
          <Typography variant="h6" color="inherit" className={classes.title}>
            Dictionaries
          </Typography>
        </Toolbar>
      </AppBar>
    );
  }
}

export default withStyles(styles)(DictionaryToolbar);
