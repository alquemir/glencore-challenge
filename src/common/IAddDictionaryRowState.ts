interface IAddDictionaryRowState {
  rowFrom: string;
  rowTo: string;
}

export { IAddDictionaryRowState };
