import { Map } from 'immutable';

interface IDictionary {
  id: string;
  name: string;
  rows: Map<string, string>;
}

export { IDictionary };
