interface IAddDictionaryProps {
  onAdd: (name: string) => void;
}

export { IAddDictionaryProps };
