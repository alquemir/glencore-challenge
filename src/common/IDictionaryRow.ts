interface IDictionaryRow {
  from: string;
  to: string;
}

export { IDictionaryRow };
