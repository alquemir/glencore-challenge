import { IDictionaryRow } from './IDictionaryRow';

interface IAddDictionaryRowProps {
  onAdd: (row: IDictionaryRow) => void;
  validate: (row: IDictionaryRow) => boolean;
}

export { IAddDictionaryRowProps };
