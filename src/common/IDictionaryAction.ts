import { IDictionaryActionPayload } from './IDictionaryActionPayload';

interface IDictionaryAction {
  type: string;
  payload: IDictionaryActionPayload;
}

export { IDictionaryAction };
