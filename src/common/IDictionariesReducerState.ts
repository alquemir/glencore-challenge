import { List } from 'immutable';
import { IDictionary } from './IDictionary';

interface IDictionariesReducerState {
  selectedDictionaryId: string;
  dictionaries: List<IDictionary>;
}

export { IDictionariesReducerState };
