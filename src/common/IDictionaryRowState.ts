interface IDictionaryRowState {
  isEditting: boolean;
  rowFrom: string;
  rowTo: string;
}

export { IDictionaryRowState };
