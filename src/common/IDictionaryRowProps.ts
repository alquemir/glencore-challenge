import { IDictionaryRow } from './IDictionaryRow';

interface IDictionaryRowProps {
  row: IDictionaryRow;
  onEdit: (row: IDictionaryRow) => void;
  onDelete: (rowKey: string) => void;
}

export { IDictionaryRowProps };
