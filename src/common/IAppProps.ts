import { List } from 'immutable';
import { IDictionary } from './IDictionary';
import { IActionProps } from './IActionProps';

interface IAppProps extends IActionProps {
  dictionaries: List<IDictionary>;
  selectedDictionary: IDictionary | undefined;
}

export { IAppProps };
