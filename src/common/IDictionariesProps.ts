import { List } from 'immutable';
import { IDictionary } from './IDictionary';
import { IActionProps } from './IActionProps';

interface IDictionariesProps extends IActionProps {
  classes: any;
  dictionaries: List<IDictionary>;
  selectedDictionary: IDictionary | undefined;
}

export { IDictionariesProps };
