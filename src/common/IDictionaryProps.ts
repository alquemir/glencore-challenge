import { IDictionary } from './IDictionary';
import { IActionProps } from './IActionProps';

interface IDictionaryProps extends IActionProps {
  dictionary: IDictionary;
  isSelected: boolean;
}

export { IDictionaryProps };
