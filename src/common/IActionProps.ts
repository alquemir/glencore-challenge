import { IDictionary } from './IDictionary';
import { IDictionaryRow } from './IDictionaryRow';

interface IActionProps {
  selectedDictionary: IDictionary | undefined;
  setSelectedDictionary: (dictionaryId: string) => any;
  addDictionary: (dictionaryName: string) => any;
  removeDictionary: (dictionaryId: string) => any;
  addRowToDictionary: (dictionaryId: string, row: IDictionaryRow) => any;
  deleteRowFromDictionary: (dictionaryId: string, rowKey: string) => any;
  updateRowInDictionary: (dictionaryId: string, row: IDictionaryRow) => any;
}

export { IActionProps };
