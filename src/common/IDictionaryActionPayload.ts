import { IDictionaryRow } from './IDictionaryRow';
import { IDictionary } from './IDictionary';

interface IDictionaryActionPayload {
  dictionaryId?: string;
  dictionaryName?: string;
  rowKey?: string;
  row?: IDictionaryRow;
  dictionary?: IDictionary;
}

export { IDictionaryActionPayload };
