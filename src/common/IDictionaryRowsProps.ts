import { IDictionary } from './IDictionary';
import { IDictionaryRow } from './IDictionaryRow';

interface IDictionaryRowsProps {
  dictionary: IDictionary;
  onEdit: (row: IDictionaryRow) => void;
  onAdd: (row: IDictionaryRow) => void;
  onDelete: (rowKey: string) => void;
}

export { IDictionaryRowsProps };
