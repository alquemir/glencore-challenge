import { IDictionaryAction, IDictionaryRow } from 'common';

export const ActionTypes = {
  SET_SELECTED_DICTIONARY: 'SET_SELECTED_DICTIONARY',
  ADD_DICTIONARY: 'ADD_DICTIONARY',
  REMOVE_DICTIONARY: 'REMOVE_DICTIONARY',
  ADD_ROW_TO_DICTIONARY: 'ADD_ROW_TO_DICTIONARY',
  UPDATE_ROW_IN_DICTIONARY: 'UPDATE_ROW_IN_DICTIONARY',
  DELETE_ROW_FROM_DICTIONARY: 'DELETE_ROW_FROM_DICTIONARY'
};

export const setSelectedDictionary = (
  dictionaryId: string
): IDictionaryAction => ({
  type: ActionTypes.SET_SELECTED_DICTIONARY,
  payload: { dictionaryId }
});

export const addDictionary = (dictionaryName: string): IDictionaryAction => ({
  type: ActionTypes.ADD_DICTIONARY,
  payload: { dictionaryName }
});

export const removeDictionary = (dictionaryId: string): IDictionaryAction => ({
  type: ActionTypes.REMOVE_DICTIONARY,
  payload: { dictionaryId }
});

export const addRowToDictionary = (
  dictionaryId: string,
  row: IDictionaryRow
) => ({
  type: ActionTypes.ADD_ROW_TO_DICTIONARY,
  payload: { dictionaryId, row }
});

export const deleteRowFromDictionary = (
  dictionaryId: string,
  rowKey: string
) => ({
  type: ActionTypes.DELETE_ROW_FROM_DICTIONARY,
  payload: { dictionaryId, rowKey }
});

export const updateRowInDictionary = (
  dictionaryId: string,
  row: IDictionaryRow
) => ({
  type: ActionTypes.UPDATE_ROW_IN_DICTIONARY,
  payload: { dictionaryId, row }
});
