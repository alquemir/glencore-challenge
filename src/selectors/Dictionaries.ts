import { createSelector } from 'reselect';
import { List } from 'immutable';
import { IDictionary, IDictionariesReducerState } from 'common';

const getDictionariesFromState = (
  state: IDictionariesReducerState
): List<IDictionary> => state.dictionaries;

const getSelectedDictionaryFromState = (
  state: IDictionariesReducerState
): string => state.selectedDictionaryId;

export const getSelectedDictionary = createSelector(
  getDictionariesFromState,
  getSelectedDictionaryFromState,
  (
    dictionaries: List<IDictionary>,
    selectedDictionaryId: string
  ): IDictionary | undefined =>
    dictionaries.find(dictionary => dictionary.id === selectedDictionaryId)
);
