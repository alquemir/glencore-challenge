import { List, Map } from 'immutable';
import { getSelectedDictionary } from '../Dictionaries';

describe('dictionaries selectors', () => {
  it('should be able to fetch the selected dictionary', () => {
    const state = {
      selectedDictionaryId: 'dictionary-2',
      dictionaries: List([
        {
          id: 'dictionary-1',
          name: 'Colors',
          rows: Map({
            Anthracite: 'Dark Grey',
            'Midnight Black': 'Black',
            'Mystic Silver': 'Silver'
          })
        },
        {
          id: 'dictionary-2',
          name: 'Foobar',
          rows: Map({
            Foo: 'Foo',
            Bar: 'Bar',
            Baz: 'Baz'
          })
        }
      ])
    };

    expect(getSelectedDictionary(state)).toEqual(state.dictionaries.get(1));
  });
});
