import { IDictionary, IDictionaryRow } from 'common';

const isValidDictionaryRow = (dictionary: IDictionary) => (
  row: IDictionaryRow
) => !dictionary.rows.has(row.from);

export { isValidDictionaryRow };
