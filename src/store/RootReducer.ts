import { combineReducers } from 'redux';
import { dictionaries } from 'reducers';

const rootReducer = combineReducers({
  dictionaries
});

export { rootReducer };
