import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import { rootReducer } from './RootReducer';
import thunk from 'redux-thunk';

const logger = createLogger();

const configureStore = () =>
  createStore(rootReducer, applyMiddleware(thunk, logger));

export { configureStore };
