import { List, Map } from 'immutable';
import { IDictionary, IDictionaryActionPayload } from 'common';

const addDictionary = (
  dictionaries: List<IDictionary>,
  actionPayload: IDictionaryActionPayload
): List<IDictionary> =>
  actionPayload != null && actionPayload.dictionaryName != null
    ? dictionaries.push({
        id: `dictionary-${dictionaries.size + 1}`,
        name: actionPayload.dictionaryName,
        rows: Map<string, string>()
      })
    : dictionaries;

export { addDictionary };
