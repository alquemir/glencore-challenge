import { List } from 'immutable';
import { IDictionary, IDictionaryActionPayload } from 'common';

const getDictionaryIndexByPayloadId = (
  dictionaries: List<IDictionary>,
  actionPayload: IDictionaryActionPayload
) =>
  actionPayload != null && actionPayload.dictionaryId != null
    ? dictionaries.findIndex(value => value.id === actionPayload.dictionaryId)
    : -1;

export { getDictionaryIndexByPayloadId };
