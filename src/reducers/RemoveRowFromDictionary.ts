import { IDictionary, IDictionaryActionPayload } from 'common';

const removeRowFromDictionary = (
  dictionary: IDictionary,
  actionPayload: IDictionaryActionPayload
): IDictionary =>
  actionPayload != null && actionPayload.rowKey != null
    ? {
        id: dictionary.id,
        name: dictionary.name,
        rows: dictionary.rows.delete(actionPayload.rowKey)
      }
    : dictionary;

export { removeRowFromDictionary };
