import { ActionTypes } from 'actions/Dictionaries';
import { IDictionaryAction, IDictionariesReducerState } from 'common';
import { addRowToDictionary } from './AddRowToDictionary';
import { updateRowOfDictionary } from './UpdateRowOfDictionary';
import { removeRowFromDictionary } from './RemoveRowFromDictionary';
import { addDictionary } from './AddDictionary';
import { getDictionaryIndexByPayloadId } from './GetDictionaryIndexByPayloadId';
import { mockDictionaries } from 'mocks';

const initialState: IDictionariesReducerState = {
  selectedDictionaryId: '',
  dictionaries: mockDictionaries
};

const dictionaries = (
  state = initialState,
  action: IDictionaryAction
): IDictionariesReducerState => {
  switch (action.type) {
    case ActionTypes.SET_SELECTED_DICTIONARY: {
      const { payload } = action;
      return {
        ...state,
        selectedDictionaryId: payload.dictionaryId || ''
      };
    }
    case ActionTypes.ADD_DICTIONARY: {
      return {
        ...state,
        dictionaries: addDictionary(state.dictionaries, action.payload)
      };
    }
    case ActionTypes.REMOVE_DICTIONARY: {
      return {
        ...state,
        dictionaries: state.dictionaries.filter(
          value => value.id !== action.payload.dictionaryId
        )
      };
    }
    case ActionTypes.ADD_ROW_TO_DICTIONARY: {
      const { dictionaries } = state;
      const { payload } = action;
      const dictionary = dictionaries.find(
        value => value.id === payload.dictionaryId
      );

      return {
        ...state,
        dictionaries:
          dictionary != null
            ? dictionaries.set(
                getDictionaryIndexByPayloadId(dictionaries, payload),
                addRowToDictionary(dictionary, payload)
              )
            : dictionaries
      };
    }
    case ActionTypes.DELETE_ROW_FROM_DICTIONARY: {
      const { dictionaries } = state;
      const { payload } = action;
      const dictionary = dictionaries.find(
        value => value.id === payload.dictionaryId
      );

      return {
        ...state,
        dictionaries:
          dictionary != null
            ? dictionaries.set(
                getDictionaryIndexByPayloadId(dictionaries, payload),
                removeRowFromDictionary(dictionary, payload)
              )
            : dictionaries
      };
    }
    case ActionTypes.UPDATE_ROW_IN_DICTIONARY: {
      const { dictionaries } = state;
      const { payload } = action;
      const dictionary = dictionaries.find(
        value => value.id === payload.dictionaryId
      );

      return {
        ...state,
        dictionaries:
          dictionary != null
            ? dictionaries.set(
                getDictionaryIndexByPayloadId(dictionaries, payload),
                updateRowOfDictionary(dictionary, payload)
              )
            : dictionaries
      };
    }
    default:
      return state;
  }
};

export { dictionaries };
