import { IDictionary, IDictionaryActionPayload } from 'common';

const addRowToDictionary = (
  dictionary: IDictionary,
  actionPayload: IDictionaryActionPayload
): IDictionary =>
  actionPayload != null && actionPayload.row != null
    ? {
        id: dictionary.id,
        name: dictionary.name,
        rows: dictionary.rows.set(actionPayload.row.from, actionPayload.row.to)
      }
    : dictionary;

export { addRowToDictionary };
