import { IDictionary, IDictionaryActionPayload } from 'common';

const updateRowOfDictionary = (
  dictionary: IDictionary,
  actionPayload: IDictionaryActionPayload
) => {
  if (actionPayload == null || actionPayload.row == null) return dictionary;
  return dictionary.rows.has(actionPayload.row.from)
    ? {
        id: dictionary.id,
        name: dictionary.name,
        rows: dictionary.rows.set(actionPayload.row.from, actionPayload.row.to)
      }
    : dictionary;
};

export { updateRowOfDictionary };
