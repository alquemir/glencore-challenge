import { List, Map } from 'immutable';
import { ActionTypes } from 'actions/Dictionaries';
import { dictionaries } from 'reducers';

describe('dictionaries reducer', () => {
  it('should be possible to select a dictionary', () => {
    const initialState = {
      selectedDictionaryId: '',
      dictionaries: List([
        {
          id: 'dictionary-1',
          name: 'Colors',
          rows: Map({
            Anthracite: 'Dark Grey',
            'Midnight Black': 'Black',
            'Mystic Silver': 'Silver'
          })
        },
        {
          id: 'dictionary-2',
          name: 'Foobar',
          rows: Map({
            Foo: 'Foo',
            Bar: 'Bar',
            Baz: 'Baz'
          })
        }
      ])
    };

    const action = {
      type: ActionTypes.SET_SELECTED_DICTIONARY,
      payload: { dictionaryId: 'dictionary-2' }
    };

    const state = dictionaries(initialState, action);
    expect(state.selectedDictionaryId).toBe('dictionary-2');
  });

  it('should be possible to create a new dictionary', () => {
    const initialState = {
      selectedDictionaryId: '',
      dictionaries: List()
    };

    const action = {
      type: ActionTypes.ADD_DICTIONARY,
      payload: { dictionaryName: 'Countries' }
    };

    const state = dictionaries(initialState, action);
    expect(state.dictionaries.size).toBe(1);
    expect(state.dictionaries.get(0)!.name).toBe('Countries');
  });

  it('should be possible to delete an existing dictionary by providing the corresponding id', () => {
    const initialState = {
      selectedDictionaryId: '',
      dictionaries: List([
        {
          id: 'dictionary-1',
          name: 'Colors',
          rows: Map({
            Anthracite: 'Dark Grey',
            'Midnight Black': 'Black',
            'Mystic Silver': 'Silver'
          })
        },
        {
          id: 'dictionary-2',
          name: 'Foobar',
          rows: Map({
            Foo: 'Foo',
            Bar: 'Bar',
            Baz: 'Baz'
          })
        }
      ])
    };

    const action = {
      type: ActionTypes.REMOVE_DICTIONARY,
      payload: { dictionaryId: 'dictionary-1' }
    };

    const state = dictionaries(initialState, action);
    expect(state.dictionaries.size).toBe(1);
    expect(state.dictionaries.get(0)!.id).toBe('dictionary-2');
  });

  it('should be possible to add a row to an existing dictionary', () => {
    const initialState = {
      selectedDictionaryId: '',
      dictionaries: List([
        {
          id: 'dictionary-1',
          name: 'Colors',
          rows: Map({
            Anthracite: 'Dark Grey',
            'Midnight Black': 'Black',
            'Mystic Silver': 'Silver'
          })
        }
      ])
    };

    const action = {
      type: ActionTypes.ADD_ROW_TO_DICTIONARY,
      payload: {
        dictionaryId: 'dictionary-1',
        row: { from: 'Ocean Blue', to: 'Blue' }
      }
    };

    const state = dictionaries(initialState, action);
    expect(state.dictionaries.get(0)!.rows.size).toBe(4);
    expect(state.dictionaries.get(0)!.rows.get('Ocean Blue')).toBe('Blue');
  });

  it('should not be possible to add duplicate rows to an existing dictionary', () => {
    const initialState = {
      selectedDictionaryId: '',
      dictionaries: List([
        {
          id: 'dictionary-1',
          name: 'Colors',
          rows: Map({
            Anthracite: 'Dark Grey',
            'Midnight Black': 'Black',
            'Mystic Silver': 'Silver'
          })
        }
      ])
    };

    const action = {
      type: ActionTypes.ADD_ROW_TO_DICTIONARY,
      payload: {
        dictionaryId: 'dictionary-1',
        row: { from: 'Midnight Black', to: 'Black' }
      }
    };

    const state = dictionaries(initialState, action);
    expect(state.dictionaries.get(0)!.rows.size).toBe(3);
  });

  it('should be possible to remove a row from an existing dictionary', () => {
    const initialState = {
      selectedDictionaryId: '',
      dictionaries: List([
        {
          id: 'dictionary-1',
          name: 'Colors',
          rows: Map({
            Anthracite: 'Dark Grey',
            'Midnight Black': 'Black',
            'Mystic Silver': 'Silver'
          })
        }
      ])
    };

    const action = {
      type: ActionTypes.DELETE_ROW_FROM_DICTIONARY,
      payload: {
        dictionaryId: 'dictionary-1',
        rowKey: 'Midnight Black'
      }
    };

    const state = dictionaries(initialState, action);
    expect(state.dictionaries.get(0)!.rows.size).toBe(2);
    expect(state.dictionaries.get(0)!.rows.has('Midnight Black')).toBe(false);
  });

  it('should be possible to update the row of an existing dictionary', () => {
    const initialState = {
      selectedDictionaryId: '',
      dictionaries: List([
        {
          id: 'dictionary-1',
          name: 'Colors',
          rows: Map({
            Anthracite: 'Dark Grey',
            'Midnight Black': 'Black',
            'Mystic Silver': 'Silver'
          })
        }
      ])
    };

    const action = {
      type: ActionTypes.UPDATE_ROW_IN_DICTIONARY,
      payload: {
        dictionaryId: 'dictionary-1',
        row: { from: 'Midnight Black', to: 'White' }
      }
    };

    const state = dictionaries(initialState, action);
    expect(state.dictionaries.get(0)!.rows.get('Midnight Black')).toBe('White');
  });
});
