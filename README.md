This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app) for the Glencore challenge

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Table of Contents

- [How to run the application](#how-to-run-the-application)
- [How to run tests](#how-to-run-tests)

## How to run the application

1. Run `npm install` to install the necessary `npm` dependencies
2. Run `npm start` to start the application
3. The application should launch automatically, otherwise navigate to `http://localhost:3000`

## How to run tests

1. Run `npm run test`
